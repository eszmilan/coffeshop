package com.coffe;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.coffe.model.Product;
import com.coffe.repository.CustomerRepository;
import com.coffe.repository.OrderRepository;
import com.coffe.repository.ProductRepository;

@SpringBootApplication
public class SpringcoffeeshopApplication implements CommandLineRunner {

	@Autowired
    ProductRepository productRepository;

	@Autowired
    CustomerRepository customerRepository;

	@Autowired
    OrderRepository orderRepository;



    public static void main(String[] args) {
		SpringApplication.run(SpringcoffeeshopApplication.class, args);
	}


    @Override
    public void run(String... strings) throws Exception {

        Product coffe = new Product();
        coffe.setProductName("Coffe");
        coffe.setProductPrice(3.95);

        Product capuccinno = new Product();
        capuccinno.setProductName("Capuccinno");
        capuccinno.setProductPrice(4.95);
        
        Product latte = new Product();
        latte.setProductName("Latte");
        latte.setProductPrice(3.95);
        
        Product melange = new Product();
        melange.setProductName("Melange");
        melange.setProductPrice(3.95);
        
        productRepository.save(coffe);
        productRepository.save(capuccinno);
        productRepository.save(latte);
        productRepository.save(melange);

    }


}
