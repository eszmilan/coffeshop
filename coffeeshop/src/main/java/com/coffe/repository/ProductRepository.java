package com.coffe.repository;

import org.springframework.data.repository.CrudRepository;

import com.coffe.model.Product;


public interface ProductRepository extends CrudRepository<Product, Long> {

}




