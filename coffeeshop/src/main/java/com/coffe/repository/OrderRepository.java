package com.coffe.repository;

import org.springframework.data.repository.CrudRepository;

import com.coffe.model.CustomerOrder;


public interface OrderRepository extends CrudRepository<CustomerOrder,Long>{

}
