package com.coffe.repository;

import org.springframework.data.repository.CrudRepository;

import com.coffe.model.Customer;


public interface CustomerRepository extends CrudRepository<Customer, Long>{

}
